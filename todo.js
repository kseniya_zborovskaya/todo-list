const TASK_LIST = 'taskslist';

var ul = document.querySelector('ul');

const todos = JSON.parse(localStorage.getItem(TASK_LIST)) ?? [];

function saveTodos(todos) {
  localStorage.setItem(TASK_LIST, JSON.stringify(todos));
}

function newElement() {
  var inputValue = document.querySelector('input').value;
  const randomId = Math.random().toString();
  if (inputValue.length !== 0) {
    todos.push({
      text: inputValue,
      done: false,
      id: randomId,
    });
    saveTodos(todos);
  }

  var li = document.createElement('li');
  li.classList.add('list__item');
  li.setAttribute('id', randomId);
  li.appendChild(document.createTextNode(inputValue));
  document.querySelector('ul').appendChild(li);
  document.querySelector('input').value = '';
}

function todosRender() {
  if (todos) {
    for (todo of todos) {
      var li = document.createElement('li');
      li.classList.add('list__item');
      li.appendChild(document.createTextNode(todo.text));
      li.setAttribute('id', todo.id);
      document.querySelector('ul').appendChild(li);
      if (todo.done) {
        li.classList.toggle('list__item_done');
      }
    }
  }
}


function toggleTodo(todo) {
  if (todo) {
    todo.done = !todo.done;
  }
}

function toggleDomTodo(event) {
  const selectedTodoDomElement = event.target;
  if (selectedTodoDomElement.classList.contains('list__item')) {
    selectedTodoDomElement.classList.toggle('list__item_done');

    const todoId = event.target.getAttribute('id');
    const foundTodo = todos.find((todo) => todo.id === todoId);
    toggleTodo(foundTodo);
    saveTodos(todos);
  }
}

document.addEventListener('click', toggleDomTodo);
